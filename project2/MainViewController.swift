//
//  MainViewController.swift
//  project2
//
//  Created by Алексей Трофимов on 31.08.2022.
//

import UIKit

class MainViewController: UIViewController {
    
    var myTableView = UITableView()
    var mySegmentedControl = UISegmentedControl()
    let identifier = "cell"
    let items = ["People", "Cars"]
    var list: [List] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .blue
        setupTableView()
        setupSegmentedControl()
    }
    
    func setupTableView(){
        
        let frame = CGRect(x: 0, y: 150, width: view.bounds.width, height: view.bounds.height-150)
        myTableView = UITableView(frame: frame, style: .plain)
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
        myTableView.delegate = self
        myTableView.dataSource = self
        view.addSubview(myTableView)
    }
    
    func setupSegmentedControl(){
        
        mySegmentedControl = UISegmentedControl(items: items)
        mySegmentedControl.frame = CGRect(x: 20, y: 80, width: view.bounds.width-40, height: 50)
        mySegmentedControl.backgroundColor = .gray
        view.addSubview(mySegmentedControl)
        mySegmentedControl.addTarget(self, action: #selector(selectSegment), for: .valueChanged)
    }
    
    @objc func selectSegment(target: UISegmentedControl){
        let segment = target.selectedSegmentIndex
        switch segment {
        case 0:
            list = List.getList1()
            myTableView.reloadData()
        case 1:
            list = List.getList2()
            myTableView.reloadData()
        default:
            break
        }
    }
    
}


extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = myTableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath)
        cell.textLabel?.text = list[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showAlert(indexPath: indexPath)
    }
    
}



extension MainViewController {
    
    func showAlert(indexPath: IndexPath){
        let message = list[indexPath.row].name
        let alertController = UIAlertController(title: "your choice", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel)
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
    
}
