//
//  Model.swift
//  project2
//
//  Created by Алексей Трофимов on 31.08.2022.
//

import Foundation


struct List {
    
    let name: String
    
    static func getList1() -> [List] {
        return [
            List(name: "Teylor Swift"),
            List(name: "Tim Cook"),
            List(name: " Steve Jobs")
        ]
    }
    
    static func getList2() -> [List] {
        return [
            List(name: "Mercedes"),
            List(name: "BMW"),
            List(name: "Audi")
        ]
    }
}


